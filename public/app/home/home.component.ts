import { Component, OnInit } from '@angular/core';
import { DataService } from '../data/data.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data: Array<number> = [];

  constructor(
    private dataService: DataService,
  ) {}

  ngOnInit() {}

  loadData() {
      this.dataService.getUsers().subscribe(
          data => this.data = data,
          error => console.log("Error fetching data: ", error)
      )
  }

}