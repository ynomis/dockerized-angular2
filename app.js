/*
 Main Express entrypoint to be used in production
*/
let express     = require('express'),
    // compression = require('compression'),
    path        = require('path'),
    morgan      = require('morgan'),
    config      = require('./server/config'),
    http        = require('http'),
    bodyParser  = require('body-parser'),

    app         = express();

// Gzip compression for performance boost
// app.use(compression());

// Logs all requests to STDOUT
app.use(morgan('combined'));

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Get our API routes
const api = require('./server/routes');

// Set our api routes
app.use('/', api);

// Serve static files
app.use( express.static(path.join( __dirname, 'public/dist' )));

// Start server
http.createServer(app).listen(config.PORT);
