var User = require('../models/User');

// Authenticate a PER user, eventually from OKTA
exports.auth = function(req, res, next) {
    res.send('Yo, whats up?');
}

exports.all = function(req, res, next) {
    User.find({}, (err, users) => {
        if (err) res.status(500).send(error)

        res.status(200).json(users);
    });
}

exports.one = function(req, res, next) {
    console.log('debug::req.param.id: ' + req.param('id'));

    User.findById(req.param('id'), (err, users) => {
        if (err) res.status(500).send(error)
        res.status(200).json(users);
    });
}

// Create a new user upon first visit
exports.create = function(req, res, next) {
    let user = new User({
        name: req.body.name,
        age: req.body.age
    });

    user.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'User created successfully'
        });
    });
}