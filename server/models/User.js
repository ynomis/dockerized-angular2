var mongoose = require("mongoose");

// create mongoose schema
var UserSchema = new mongoose.Schema({
  name: String,
  age: Number
});

// create mongoose model
module.exports = mongoose.model('User', UserSchema); 