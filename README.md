# Project Overview
This is the initial part of a frontend project. It is a full web stack build on Angular2, Express, MongoDB and some others. It was forked from a [GitHub project](https://github.com/tvm18860/angular2-express-seed). Additions are:

* Connected MongoDB as persistent layer in docker for both dev and production;
* Swapped Material Design with another package, Covalant;
* Use SASS as main styling language, instead of CSS;
* Added authentication middleware in Express for api access;
* Added deployment scripts for AWS ECS/ECR through Jenkins;
 
## Docker cheatsheet:

launch a new docker container (of mongo):

```docker run --name mongodb-local -v mongo-data:/data/db -p 27017:27017 -d mongo```
 
stop an existing container:

```docker stop mongodb-local```

```docker stop angular2-dev local```

start a existing container:

```docker start mongodb-local```

```docker start angular2-dev```
 
get into the shell of the launched container:

```docker exec -it mongodb-local bash```

```docker exec -it angular2-dev bash```

stop all containers:

```docker kill $(docker ps -q)```
 
remove all containers:

```docker rm $(docker ps -a -q)```
 
remove all docker images:

```docker rmi $(docker images -q)```

once you pull in the latest change from github, you will have to rebuild the image, and here is the command:

```docker-compose up --build```

Or

```
docker-compose build
docker-compose up
```

This will update the current setup to the latest, which also leaves a lot of stale builds. 

